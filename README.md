## Installation & Test Run
- Clone this repo.

    - `git clone https://gitlab.com/Kcanyucel/blog-cucumber-ci-2.git`


- Run this command `mvn clean install` on main folder.

- You shouldn't have any checkstyle violation problem to execute `mvn clean test`.

- On your IDE, you can run tests via feature files. By default, spring glue is added to glue. In order to prevent any error from spring glue, our glue on configuration should be like this:

    - in.reqres.api.automation.steps
    
- If you are using Java version 11 as project sdk, you can run tests via feature files with right click + run

## In case there is a problem
  - Try to do
    - `export MAVEN_OPTS=--add-opens=java.base/java.util=ALL-UNNAMED`
    - `mvn clean install`
  - If not solved, check java version 11 to use.

## Reporting
  - After running tests, you can access reports at /target/cucumber-html-reports path
