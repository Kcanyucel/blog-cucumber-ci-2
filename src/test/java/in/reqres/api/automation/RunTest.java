package in.reqres.api.automation;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@CucumberOptions(
    features = "src/test/resources/features",
    plugin = {"pretty", "json:target/cucumber.json"},
    glue = {"in/reqres/api/automation/steps"},
    tags = {"@RegisterSuccessfully, @RegisterUnSuccessfully, @LoginUnSuccessfully, @LoginSuccessfully"} )

@RunWith(Cucumber.class)
public class RunTest {}
