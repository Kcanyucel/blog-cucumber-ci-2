package in.reqres.api.automation.steps;

import cucumber.api.java.en.*;
import gherkin.deps.com.google.gson.JsonObject;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;

import static io.restassured.RestAssured.given;

public class LoginSuccessfullyStep {

  Response response;
  private final static String email = "eve.holt@reqres.in";
  private final static String password = "cityslicka";

  @Given("^define to base uri and base path for login$")
  public void define_to_base_uri_and_base_path_for_login() {
    RestAssured.baseURI = "https://reqres.in/";
    RestAssured.basePath = "/api/login";

  }

  @When("^send request body to login$")
  public void send_request_body_to_login() {
    JsonObject userBody = new JsonObject();
    userBody.addProperty("email", email);
    userBody.addProperty("password", password);

    response = given().
        contentType(ContentType.JSON).
        when().
        body(userBody.toString()).
        post().
        then().
        statusCode(200).
        extract().
        response().prettyPeek();
  }

  @Then("^validate to response for login$")
  public void validate_to_response_for_login() {
    String accessToken = response.path("token");

    Assert.assertNotNull("token is null!", accessToken);
  }
}
