package in.reqres.api.automation.steps;

import cucumber.api.java.en.*;
import gherkin.deps.com.google.gson.JsonObject;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;

import static io.restassured.RestAssured.given;

public class RegisterSuccessfullyStep {

  Response response;
  private final static String email = "eve.holt@reqres.in";
  private final static String password = "pistol";

  @Given("^define to base uri and base path for register$")
  public void define_to_base_uri_and_base_path_for_register() {
    RestAssured.baseURI = "https://reqres.in/";
    RestAssured.basePath = "/api/register";
  }

  @When("^send request body to register$")
  public void send_request_body_to_register() {
    JsonObject userBody = new JsonObject();
    userBody.addProperty("email", email);
    userBody.addProperty("password", password);

    response = given().
        contentType(ContentType.JSON).
        when().
        body(userBody.toString()).
        post().
        then().
        statusCode(200).
        extract().
        response().prettyPeek();
  }

  @Then("^validate to response for register$")
  public void validate_to_response_for_register() {
    int id = response.path("id");
    String accessToken = response.path("token");

    Assert.assertNotNull("token is null!", accessToken);
    Assert.assertNotNull("token is null!", id);
  }
}
