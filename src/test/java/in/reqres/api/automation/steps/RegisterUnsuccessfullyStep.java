package in.reqres.api.automation.steps;

import cucumber.api.java.en.*;
import gherkin.deps.com.google.gson.JsonObject;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;

import static io.restassured.RestAssured.given;

public class RegisterUnsuccessfullyStep {

  Response response;
  private final static String email = "sydney@fife";
  private final static String errorMessage = "Missing password";

  @Given("^define to base uri and base path for unsuccessfully register$")
  public void define_to_base_uri_and_base_path_for_unsuccessfully_register() {
    RestAssured.baseURI = "https://reqres.in/";
    RestAssured.basePath = "/api/register";
  }

  @When("^send request body to unsuccessfully register$")
  public void send_request_body_to_unsuccessfully_register() {
    JsonObject userBody = new JsonObject();
    userBody.addProperty("email", email);

    response = given().
        contentType(ContentType.JSON).
        when().
        body(userBody.toString()).
        post().
        then().
        statusCode(400).
        extract().
        response().prettyPeek();
  }

  @Then("^validate to response for unsuccessfully register$")
  public void validate_to_response_for_unsuccessfully_register() {
    String error = response.path("error");

    Assert.assertEquals("errorMessage not true !", errorMessage, error);
  }
}
